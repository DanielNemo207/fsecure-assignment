# Fsecure assignemt: 
Implement a simple message board web application.

## Repo
- This is repository of Fsecure assignment app
- Run this command to clone the repo:
```
git clone git@gitlab.com:DanielNemo207/fsecure-assignment.git
cd fsecure-assignment
```

## Quick start
- Create virtualenv (note: this project requires you to have python3.6+):
```
python3 -m venv ../venv
```

- Install dependencies:

```
../venv/bin/pip install -r requirements.txt
```

- Run API app in local:

```
../venv/bin/python -W ignore -m manage
```

## App services/APIs
- CreateMessage service - POST method REST API:
```
URL: <host>/RESTApi/v1/message/save
Body: {
    'title': <title>, (max len is 30)
    'content': <content>, (max len is 100)
    'sender': <sender>, (max len is 30)
    'url': <url> (must be a valid url)
}
```
Example in local run:
```
URL: http://localhost:5000/RESTApi/v1/message/save
Body: {
    'title': 'title 1,
    'content': 'content 1',
    'sender': 'sender 1',
    'url': 'https://www.testurl1.com/'
}
```

- ListMessages service - GET method REST API:
```
URL: <host>/RESTApi/v1/message/load?version=<version_option>&rep_format=<response_format_option>
```

Options for <version_option>:
```
- full_info
- no_url
```

Options for <response_format_option> (this is available if only the version_option is full_info):
```
- json
- xml
```


Example in local run:
```
URL: http://localhost:5000/RESTApi/v1/message/load?version=full_info&rep_format=xml
URL: http://localhost:5000/RESTApi/v1/message/load?version=full_info&rep_format=json
URL: http://localhost:5000/RESTApi/v1/message/load?version=no_url
```



## Subdirectories
* **app**: Contains all app's features
* **data**: Store the needed data files
* **requirements.txt**: Define necessary packages
* **manage.py**: Flask manager