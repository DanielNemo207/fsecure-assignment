import sqlite3
from sqlite3 import Error

from datetime import datetime

from app.setting.config import CONFIGS as cf


class SQLiteDB():

    def __init__(self):
        self.make_connection()

    def make_connection(self):
        """
        Connect to sqlite data file
        """
        try:
            self.connection = sqlite3.connect(cf.SQLITE_FILE_PATH)
            self.connection.row_factory = self.dict_factory
            # print(sqlite3.version)
        except Error as e:
            print(e)

    def execute_query(self, query):
        """
        Execute sql queries which get data from database
        """
        try:
            cur = self.connection.cursor()
            cur.execute(query)
            rows = cur.fetchall()
            return rows
        except Error as e:
            return "Query error: {}".format(e)

    def execute_command(self, command):
        """
        Execute sql commands which modifies data in database
        """
        try:
            cur = self.connection.cursor()
            cur.execute(command)
            self.connection.commit()
            return cur.lastrowid
        except Error as e:
            return "Command error: {}".format(e)

    def close(self):
        """
        Close DB connection, this should be called manually after the
        connection is used to guarantee no corner case can happen
        """
        try:
            self.connection.close()
        except:
            print('No connection to database')

    def dict_factory(self, cursor, row):
        """
        Transform sqlite.Row object to dictionary object
        """
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def insert_message(self, title, content, sender, url):
        """
        Insert a new message to database
        """
        created_at = datetime.now().replace(microsecond=0)

        query = """
            INSERT INTO message (
                title,content,sender,url,created_at,updated_at
            ) VALUES (
                '{}', '{}', '{}', '{}', '{}', '{}'
            );            
        """.format(
            str(title).replace("'", "''"),
            str(content).replace("'", "''"),
            str(sender).replace("'", "''"),
            str(url).replace("'", "''"),
            str(created_at).replace("'", "''"),
            str(created_at).replace("'", "''")
        )

        inserted_id = self.execute_command(query)
        return inserted_id

    def select_all_messages(self, cols):
        """
        List all messages in database
        """
        query = """
            SELECT {} FROM message;
        """.format(
            ",".join(cols)
        )
        messages = self.execute_query(query)
        return messages
