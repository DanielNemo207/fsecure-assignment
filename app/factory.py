from flask import Flask
from flask import jsonify
from flask_restful import Api
from flask_cors import CORS

from app.setting.config import CONFIGS


def create_app(register_stuffs=True, config=CONFIGS):
    app = Flask(__name__)
    app.config.from_object(config)

    register_rest_api(app)

    return app


def register_rest_api(app):

    from app.restApi_handler.message import CreateMessagelHandler, LoadMessagesHandler
    # import more api handler here

    version = app.config["VERSION"]
    rest_api_url = app.config["REST_API_URL"]
    api_version_url = "/{}/{}/".format(rest_api_url, version)
    api = Api(app)
    CORS(app)

    api.add_resource(CreateMessagelHandler,
                     "{}message/save".format(api_version_url))
    api.add_resource(LoadMessagesHandler,
                     "{}message/load".format(api_version_url))
    # TODO: Add more api here in the future (When the app is extended)
