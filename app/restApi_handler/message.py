from dicttoxml import dicttoxml
from xml.dom.minidom import parseString
import configparser
import re

from flask import current_app
from flask import jsonify
from flask_restful import Resource
from flask_restful.reqparse import RequestParser

from app.setting.config import CONFIGS as cf
from app.data_layer.sqlite_db import SQLiteDB


class Message_base_api(Resource):

    def __init__(self):
        # TODO: Implement other Database layers in future and replace it here
        self._db = SQLiteDB()
        super().__init__()

    def validate_url(self, url):
        """
        Validate url is in correct form
        - Return True for valid url
        - Return False for invalid url
        """
        if re.match(cf.URL_REGEX, url) is not None:
            return True
        return False


class CreateMessagelHandler(Message_base_api):

    invalidURL = 'The url is in invalid format'
    invalidLenTitle = 'The maximum length of title is {}'.format(cf.TITLE_MAX_LEN)
    invalidLenContent = 'The maximum length of title is {}'.format(cf.CONTENT_MAX_LEN)
    invalidLenSender = 'The maximum length of title is {}'.format(cf.SENDER_MAX_LEN)
    createSuccessMess = 'Successfully add the message'

    def __init__(self):
        super().__init__()

    def post(self):
        """
        Create message
        """
        parser = RequestParser()
        parser.add_argument("title", type=str, location="form", required=True)
        parser.add_argument("content", type=str,
                            location="form", required=True)
        parser.add_argument("sender", type=str, location="form", required=True)
        parser.add_argument("url", type=str, location="form", required=True)

        title = parser.parse_args()["title"]
        content = parser.parse_args()["content"]
        sender = parser.parse_args()["sender"]
        url = parser.parse_args()["url"]

        is_url_valid = self.validate_url(url)
        if not is_url_valid:
            return self.invalidURL

        if len(title) > cf.TITLE_MAX_LEN:
            return self.invalidLenTitle
        
        if len(content) > cf.CONTENT_MAX_LEN:
            return self.invalidLenContent

        if len(sender) > cf.SENDER_MAX_LEN:
            return self.invalidLenSender

        result = self._db.insert_message(title, content, sender, url)
        return result


class LoadMessagesHandler(Message_base_api):

    invalidVersion = """The version param is not valid. 
    The possible options are {}
    """.format(list(cf.ACCEPTABLE_MESSAGE_RESPONSE_VERSION_DICT.keys()))

    invalidFormat = """The format param is not valid. 
    The possible options are {}
    """.format(cf.ACCEPTABLE_MESSAGE_RESPONSE_FORMATS)

    def __init__(self):
        super().__init__()

    def get(self):
        """
        Load/List all messages
        """
        parser = RequestParser()

        parser.add_argument("version", type=str,
                            location="args", required=True)
        version = parser.parse_args()["version"]
        if version not in cf.ACCEPTABLE_MESSAGE_RESPONSE_VERSION_DICT:
            return self.invalidVersion

        is_format_required = cf.ACCEPTABLE_MESSAGE_RESPONSE_VERSION_DICT[
            version]["is_format_param_required"]
        parser.add_argument("rep_format", type=str,
                            location="args", required=is_format_required)
        rep_format = parser.parse_args()["rep_format"]

        select_cols = cf.ACCEPTABLE_MESSAGE_RESPONSE_VERSION_DICT[version]["cols"]

        # Select all messages with needed cols
        messages = self._db.select_all_messages(select_cols)

        # Format response
        if rep_format not in cf.ACCEPTABLE_MESSAGE_RESPONSE_FORMATS:
            return messages

        switcher = {
            "json": self.convert_message_list_to_json_format,
            "xml": self.convert_message_list_to_xml_format
        }
        func = switcher.get(
            rep_format, self.convert_message_list_to_default_format)
        return func(messages)

    def convert_message_list_to_json_format(self, messages):
        return jsonify(messages)

    def convert_message_list_to_xml_format(self, messages):
        xml = dicttoxml(messages)
        dom = parseString(xml)
        return dom.toprettyxml()

    def convert_message_list_to_default_format(self, messages):
        return messages

# TODO: Add new api services here in future
# class UpdateMessageHandler(Message_base_api):
# class DeleteMessageHandler(Message_base_api):
