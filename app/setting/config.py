from os import pardir
from os.path import join, abspath, dirname

import re

# Base directories
CURRENT_DIR = abspath(dirname(__file__))
APP_DIR = abspath(join(CURRENT_DIR, pardir))
ROOT_DIR = abspath(join(APP_DIR, pardir))
DATA_DIR = abspath(join(ROOT_DIR, "data"))


class BaseConfig(object):
    VERSION = "v1"

    REST_API_URL = "RESTApi"

    SQLITE_FILE_PATH = abspath(join(DATA_DIR, "sqlite_db.db"))

    TITLE_MAX_LEN = 30
    CONTENT_MAX_LEN = 100
    SENDER_MAX_LEN = 30
    
    URL_REGEX = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        # domain...
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE
    )

    ACCEPTABLE_MESSAGE_RESPONSE_VERSION_DICT = {
        "full_info": {
            "cols": ["title", "content", "sender", "url"],
            "is_format_param_required": True
        },
        "no_url": {
            "cols": ["title", "content", "sender"],
            "is_format_param_required": False
        }
    }

    ACCEPTABLE_MESSAGE_RESPONSE_FORMATS = ["json", "xml"]


CONFIGS = BaseConfig()
